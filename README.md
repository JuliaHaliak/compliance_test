### Introduction 
Prototype for text augmentation quality checks.

### Run
To run the checks you need to:

1. Use unix-based infrastructure and conda environments
2. Go to project directory
3. Create and activate conda environment from environment.yml located in project directory: 
    'conda env create -f environment.yml', 'conda activate augmentations'
   Consider environment.yml to be the list of requirements for project.
4. run
    python run_augmentations_checks.py
  or
    python run_augmentations_checks.py --check_with_embeddings

    The first default configuration will run all the checks exept the one involving building Bert-based embeddings (see point 5 below).
    Bert model will be downloaded once when you run the script for the first time only with or without check_with_embeddings option.

### Implemented checks:
1. Full augmentation equivalence to original sentence: failed if equal.
2. Full augmentation equivalence to any other augmentation: failed if equal to preserve the variety.
3. Bigrams check: average bigrams frequency is calculated and compared to empirical threshold: failed if less than threshold. Bigrams collection with frequencies is taken from this open source (https://norvig.com/ngrams/, free license) and enriched to adapt to the domain.
4. Language check: grammar, syntax and spelling check using https://pypi.org/project/language-check/ library. Failed if more than N mistakes reported, N picked empirically.
5. Embeddings cosine similarity check, runs with check_with_embeddings option: Bert-based embeddings are being built for the original sentence and the augmentation to evaluate the distance. Cosine sililarity threshold was picked empirically.
This procedure will take a while (around 22 minutes) so the basic script setting lets you skip it.

Augmentation is considered to be failed if one of checks faild.
Resulting json file is stored into result/ folder.
