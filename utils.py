#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
import os
from collections import defaultdict
import tarfile
import requests
from tqdm import tqdm

def get_bert(bert_folder, bert_url, bert_dump):
    if not os.path.isfile(bert_folder + 'saved_model.pb'):
        os.makedirs(bert_folder, exist_ok=True)
        print('Downloading Bert...')
        r = requests.get(bert_url)
        open(bert_dump, 'wb').write(r.content)
        tar = tarfile.open(bert_dump)
        print('Extracting Bert...')
        tar.extractall(bert_folder)
        tar.close()
        os.remove(bert_dump)

def load_bigrams(bigrams_file):
    bigrams = defaultdict(int)
    with open(bigrams_file) as bf:
        for line in bf:
            bigram, freq = line.strip().split('\t')
            bigrams[bigram.lower()] += int(freq)

    return bigrams

def decontracte_text(text):
    text = re.sub("won[\'’]t", "will not", text)
    text = re.sub("can[\'’]t", "can not", text)

    text = re.sub(" bout ", " about ", text)
    text = re.sub("n[\'’]t", " not", text)
    text = re.sub("[\'’]re", " are", text)
    text = re.sub("[\'’]s", " is", text)
    text = re.sub("[\'’]d", " would", text)
    text = re.sub("[\'’]ll", " will", text)
    text = re.sub("[\'’]t", " not", text)
    text = re.sub("[\'’]ve", " have", text)
    text = re.sub("[\'’]m", " am", text)
    return text