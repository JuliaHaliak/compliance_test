import json

class Parameters:
	''' Common project parameters '''

	def __init__(self, FLAGS):

		config = json.load(open(FLAGS.config))

		self.check_with_embeddings = FLAGS.check_with_embeddings

		self.data_file = config["paths"]["data_file"]
		self.result_file = config["paths"]["result_file"]
		self.result_folder = config["paths"]["result_folder"]

		self.bert_emb_size = config["general_params"]["bert_emb_size"]
		self.max_seq_len = config["general_params"]["max_seq_len"]

		self.bigrams_file = config["paths"]["bigrams_file"]
		self.bert_folder = config["paths"]["bert_folder"]
		self.bert_url = config["paths"]["bert_url"]
		self.bert_dump = config["paths"]["bert_dump"]
		self.bert_vocab = config["paths"]["bert_vocab"]
		self.min_emb_cosine_distance = config["thresholds"]["min_emb_cosine_distance"]
		self.bigrams_freq_threshold = config["thresholds"]["bigrams_freq_threshold"]
		self.max_mistakes_detected = config["thresholds"]["max_mistakes_detected"]

