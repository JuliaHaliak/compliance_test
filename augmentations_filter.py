#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
from typing import Dict, List, Optional

import copy
import nltk
import language_check
from tqdm import tqdm
from nltk.tokenize.regexp import regexp_tokenize
from sklearn.metrics.pairwise import cosine_similarity

from parameters import Parameters
from bert_embeddings import BertEmbedder
from utils import load_bigrams, decontracte_text


class AugmentationsFilter:
    """
    An abstraction for the procedures of augmentations check
    """

    def __init__(self, augmentations:Dict, params:Parameters):
        """
        Class initialisation
        Args:
            augmentations: json with augmentations loaded
            params: parameters parsed from config and flags
        """

        self.params = params
        self.bert_embedder = BertEmbedder(self.params.bert_folder,
                                          self.params.bert_vocab)

        self.bigrams_freqs = load_bigrams(self.params.bigrams_file)

        self.augmentations = augmentations
        self.checks_result = {}

    def __call__(self):
        """
        Main checks running for the set of augmentations of every original sentence
        """
        for original_sentence in tqdm(self.augmentations):
            already_accepted = set()

            self.checks_result[original_sentence] = {
                'passed': [],
                'failed': []
            }
            if self.params.check_with_embeddings:
                original_embedding, augmentation_embeddings = self._get_embeddings(original_sentence,
                                                                    self.augmentations[original_sentence])

            for ind, augmentation in enumerate(self.augmentations[original_sentence]):
                if self.params.check_with_embeddings:
                    status, check_info = self._run_checks(original_sentence, augmentation,
                                                          original_embedding, augmentation_embeddings[ind])
                else:
                    status, check_info = self._run_checks(original_sentence, augmentation)

                if augmentation in already_accepted:
                    status = 'failed'
                    check_info.append('FAILED: Already present among passed')
                else:
                    already_accepted.add(augmentation)
                    check_info.append('PASSED: Unique or first among augmentations')

                self.checks_result[original_sentence][status].append(
                    {
                        'augmentation': augmentation,
                        'checks': check_info
                    }
                )

        return self.checks_result

    def _run_checks(self, original_sentence:str, augmentation:str,
                    original_embedding:Optional[List] = None, augmentation_embedding:Optional[List] = None):
        """
        runs the checks and gathers the output of single procedures
        Args:
            original_sentence: original sentence,
            augmentation: augmentation
            original_embedding: original embedding, optional, if embedding check is used
            augmentation_embedding: augmentation embedding, optional, if embedding check is used

        Returns:
            status: status of check, passed or failed
            check_info: result description
        """
        status = 'passed'
        check_info = []
        checks = [self._check_identical, self._check_grammar_via_ngrams, self._language_check]
        if self.params.check_with_embeddings:
            checks.append(self._check_embeddnigs_distance)
        for check in checks:
            if check == self._check_embeddnigs_distance:
                original_sentence = original_embedding
                augmentation = augmentation_embedding
            current_status, current_check_info = check(original_sentence, augmentation)

            if status != current_status != 'passed':
                status = current_status

            check_info.append(current_check_info)

        return status, check_info

    def _language_check(self, _, augmentation):
        """
        Language check: grammar, syntax and spelling check
        Failed if more than max_mistakes_detected mistakes reported
        Args:
            augmentation: augmentation text
        Returns:
            status: status of check, passed or failed
            check_info: result description
        """
        tool = language_check.LanguageTool('en-US')
        mistakes = tool.check(augmentation)
        if len(mistakes) > self.params.max_mistakes_detected:
            return 'failed', 'FAILED: Too many grammatical mistakes.'
        return 'passed', 'PASSED: Grammatically correct.'

    def _get_embeddings(self, original_sentence, augmentations:List):
        """
        Gets Bert-based embeddings
        Returns:
            embedding of original sentence
            embedding of augmentation text
        """
        _augmentations = copy.deepcopy(augmentations)
        _augmentations.insert(0, original_sentence)
        embeddings = self.bert_embedder.get_embeddings(_augmentations, self.params.max_seq_len)

        return embeddings[0], embeddings[1:]


    def _check_identical(self, original_sentence:str, augmentation:str):
        """
        Full augmentation equivalence to original sentence: failed if equal.
        Args:
            original_sentence: original sentence
            augmentation: augmentation text
        Returns:
            status: status of check, passed or failed
            check_info: result description
        """
        if original_sentence == augmentation:
            return 'failed', 'FAILED: Identical to original.'
        return 'passed', 'PASSED: Different from original.'

    def _check_embeddnigs_distance(self, original_embedding:List, augmentation_embedding:List):
        """
            Embeddings cosine similarity check, runs with check_with_embeddings option:
                Bert-based embeddings are being built for the original sentence
                and the augmentation to evaluate the distance
            Args:
                original_embedding: embedding of original sentence
                augmentation: embedding of augmentation text
            Returns:
                status: status of check, passed or failed
                check_info: result description
        """

        cosine_distance = cosine_similarity(original_embedding.reshape(1, -1),
                                            augmentation_embedding.reshape(1, -1))
        if cosine_distance[0] < self.params.min_emb_cosine_distance:
            return 'failed', 'FAILED: Too far semantically: cosine distance is ' + str(cosine_distance)
        return 'passed', 'PASSED: Semantically close enough: cosine distance is ' + str(cosine_distance)


    def _check_grammar_via_ngrams(self, _, augmentation: str):
        """
        Bigrams check: average bigrams frequency is calculated and compared to empirical threshold:
        failed is less than threshold
        Args:
            augmentation: augmentation text
        Returns:
            status: status of check, passed or failed
            check_info: result description
        """
        avg_bigrams_freq = 0
        bigrams_num = 0
        augmentation = decontracte_text(augmentation)
        phrases = [phrase for phrase in re.split('[^\w\s\-]', augmentation) if phrase]
        for phrase in phrases:
            words = regexp_tokenize(phrase.lower(), pattern='\W', gaps=True)
            bigrams = [' '.join(list(bigram)) for bigram in nltk.bigrams(words)]
            bigrams_num += len(bigrams)
            for bigram in bigrams:
                if bigram in self.bigrams_freqs:
                    avg_bigrams_freq += self.bigrams_freqs[bigram]
        if bigrams_num == 0:
            return 'passed', 'PASSED: Bigrams check is skipped, too short sentence.'
        avg_bigrams_freq = avg_bigrams_freq/bigrams_num
        if avg_bigrams_freq < self.params.bigrams_freq_threshold:
            return 'failed', 'FAILED: Average bigram frequency is less than ' + \
                   str(self.params.bigrams_freq_threshold)
        return 'passed', 'PASSED: Bigrams are common enough.'
