#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import json
import os

from absl import app
from absl import flags

from augmentations_filter import AugmentationsFilter
from parameters import Parameters
from utils import get_bert


FLAGS = flags.FLAGS
flags.DEFINE_string('config', 'config/config.json', 'path to json config')
flags.DEFINE_boolean('check_with_embeddings', False, 'flag to run check with bert embeddings')

def main(argv):

    params = Parameters(FLAGS)

    get_bert(params.bert_folder, params.bert_url, params.bert_dump)

    os.makedirs(params.result_folder, exist_ok=True)
    result_file = os.path.join(params.result_folder + params.result_file)
    augmentations = json.load(open(params.data_file))
    augmentations_filter = AugmentationsFilter(augmentations, params)
    result = augmentations_filter()
    with open(result_file, "w") as write_file:
        json.dump(result, write_file, indent=4)




if __name__ == '__main__':

    app.run(main)


