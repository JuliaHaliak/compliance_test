import tensorflow_hub as hub
import tensorflow as tf
import bert_tokenization

from typing import List

class BertEmbedder():

	def __init__(self, bert_path: str, vocab_file: str):
		self.module = hub.Module(bert_path)
		self.vocab = vocab_file

		self.tokenizer = self._create_tokenizer(do_lower_case=False)

	def _create_tokenizer(self, do_lower_case=False) -> bert_tokenization.FullTokenizer:
		return bert_tokenization.FullTokenizer(vocab_file=self.vocab,
											   do_lower_case=do_lower_case)

	def _convert_sentence_to_features(self, sentence, max_seq_len) -> (List[int], List[int], List[int]):
		tokens = ['[CLS]']
		tokens.extend(self.tokenizer.tokenize(sentence))
		if len(tokens) > max_seq_len - 1:
			tokens = tokens[:max_seq_len - 1]
		tokens.append('[SEP]')

		segment_ids = [0] * len(tokens)
		input_ids = self.tokenizer.convert_tokens_to_ids(tokens)
		input_mask = [1] * len(input_ids)

		# Zero Mask till seq_length
		zero_mask = [0] * (max_seq_len - len(tokens))
		input_ids.extend(zero_mask)
		input_mask.extend(zero_mask)
		segment_ids.extend(zero_mask)

		return input_ids, input_mask, segment_ids


	def _convert_sentences_to_features(self, sentences, max_seq_len: int) -> (List[list], List[list], List[list]):
		all_input_ids = []
		all_input_mask = []
		all_segment_ids = []

		for sentence in sentences:
			input_ids, input_mask, segment_ids = self._convert_sentence_to_features(sentence, max_seq_len)
			all_input_ids.append(input_ids)
			all_input_mask.append(input_mask)
			all_segment_ids.append(segment_ids)

		return all_input_ids, all_input_mask, all_segment_ids

	def get_embeddings(self, batch: List, max_seq_len: int) -> List[list]:

		bert_sess = tf.Session()
		bert_sess.run(tf.global_variables_initializer())

		input_ids = tf.compat.v1.placeholder(dtype=tf.int32, shape=[None, None])
		input_mask = tf.compat.v1.placeholder(dtype=tf.int32, shape=[None, None])
		segment_ids = tf.compat.v1.placeholder(dtype=tf.int32, shape=[None, None])

		bert_inputs = dict(
			input_ids=input_ids,
			input_mask=input_mask,
			segment_ids=segment_ids)

		bert_outputs = self.module(bert_inputs, signature="tokens", as_dict=True)

		input_ids_vals, input_mask_vals, segment_ids_vals = self._convert_sentences_to_features(batch, max_seq_len)

		output = bert_sess.run(bert_outputs, feed_dict={input_ids: input_ids_vals,
														input_mask: input_mask_vals,
														segment_ids: segment_ids_vals})
		return output['pooled_output']
